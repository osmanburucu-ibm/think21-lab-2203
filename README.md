README to Think21 Workshop #2203 How to develop and deliver software

As a developer you need to create, change, or delete code with high quality, automated build and deployed to your development test environment.

This workshop will first introduce you to the possibilities of Agile Management with GitLab Ultimate for managing your projects using Epics, Issues, Milestones, Roadmaps and Boards. 

Then with the GitLab Flow, you will get a simple but powerful introduction into adding changes to source code, branching, and merging the changes. 

Next is an introduction into continuous integration/continuous delivery pipeline, how it works and how it can be configured.  

In an enterprise environment not every project is ready to be deployed with simple steps into production and in the next few labs we will address how to deploy complex applications using IBM UrbanCode Deploy and then how to combine both for automated pipeline for continuous delivery/deployment. 

## provide detailed information how to use this repository

## how to import artifacts

### Automated import using shell script

A simple shell script for automated importing of the artifacts into GitLab and IBM UrbanCode Deploy is provided.
You need to set the environment variables to your needs in scripts/setenv-variables.sh

Have a look at import-artifacts.sh and then run it in a shell.


### Manual import using curl

#### GitLab Artifacts
* replace xxx-YOUR-TOKEN-xxx with your Gitlab Token
* replace Think2021 as the groupname with different value if it exists (else you get an error)
* if you have a different Gitlab instance (f.e. have GitLab Ultimate on premises) then change the URL also
* 
Example:
```sh
GL_PRIVATETOKEN=xxx-YOUR-TOKEN-xxx
GL_GROUPNAME=Think2021
GL_URL=https://gitlab.com

curl --request POST --header "PRIVATE-TOKEN: ${GL_PRIVATETOKEN}" --form "name=${GL_GROUPNAME}" --form "path=${GL_GROUPNAME}" --form "file=@gitlab_artifacts/think2021_group_export.tar.gz" "$GL_URL/api/v4/groups/import"

# WAIT a few seconds to finish import
# if needed get the new Group ID
# NEWGROUPID=$(curl --header "PRIVATE-TOKEN: ${GL_PRIVATETOKEN}" "${GL_URL}"/api/v4/groups -k | jq '.[] | select (.name == "'"$GL_GROUPNAME"'" ).id')

curl --request POST --header "PRIVATE-TOKEN: ${GL_PRIVATETOKEN}" --form "namespace=${GL_GROUPNAME}" --form "name=sum-app-dev" --form "path=sum-app-dev" --form "file=@gitlab_artifacts/think2021_sum-app-dev_export.tar.gz" "$GL_URL/api/v4/projects/import"

curl --request POST --header "PRIVATE-TOKEN: ${GL_PRIVATETOKEN}" --form "namespace=${GL_GROUPNAME}" --form "name=sum-app-test" --form "path=sum-app-test" --form "file=@gitlab_artifacts/think2021_sum-app-test_export.tar.gz" "$GL_URL/api/v4/projects/import"


# if you want to check the state of the import need to store the ID
#  PRJ1ID=$(curl --request POST --header "PRIVATE-TOKEN: ${GL_PRIVATETOKEN}" --form "namespace=${GL_GROUPNAME}" --form "name=sum-app-dev" --form "path=sum-app-dev" --form "file=@gitlab_artifacts/think2021_sum-app-dev_export.tar.gz" "$GL_URL/api/v4/projects/import" | jq '.id')
# Check import status
# curl --header "PRIVATE-TOKEN: ${GL_PRIVATETOKEN}" "$GL_URL/api/v4/projects/$PRJ1ID/import" | jq '.import_status'

```

#### UrbanCode Artifacts

* If Websphere Liberty Plugin is not installed download it and install
* If UrbanCode Versioned File Storage Plugin is not at the latest level please upgrade it to version 37
* UrbanCode Deploy Plugins can be downloaded from Plug-In documentation repo https://urbancode.github.io/IBM-UCx-PLUGIN-DOCS/UCD/ or directly from the plug-in repo https://github.com/UrbanCode/IBM-UCD-PLUGINS
* it is also possible to use AUTH-TOKEN instead of username/password
* example: curl -k -u PasswordIsAuthToken:DS_AUTH_TOKEN https://myserver.example.com:8443/cli/application/info?application=JPetStore

```sh
DS_USERNAME=admin
DS_PASSWORD=admin
DS_AUTH_TOKEN=xxx-YOUR-TOKEN-xxx
DS_WEB_URL="https://192.168.72.138:8443"

wget https://github.com/UrbanCode/IBM-UCD-PLUGINS/raw/main/files/WebSphereLiberty/ucd-WebSphereLiberty-22.1134888.zip
wget https://github.com/UrbanCode/IBM-UCD-PLUGINS/raw/main/files/UrbancodeVFS/ucd-UrbancodeVFS-43.1139365.zip

curl -v -k -u "${DS_USERNAME}:${DS_PASSWORD}" -X POST -H "Content-Type: multipart/form-data" -F "file=@ucd-WebSphereLiberty-22.1134888.zip" "${DS_WEB_URL}/rest/plugin/automationPlugin"

curl -v -k -u "${DS_USERNAME}:${DS_PASSWORD}" -X POST -H "Content-Type: multipart/form-data" -F "file=@ucd-UrbancodeVFS-43.1139365.zip" "${DS_WEB_URL}/rest/plugin/automationPlugin" 

# import now the components
curl -v -k -u "${DS_USERNAME}:${DS_PASSWORD}" -X PUT -H "Content-Type: multipart/form-data" --header "Accept: application/json" -d "@urbancode_artifacts/Sum-App_components.json" "${DS_WEB_URL}/cli/component/import?disposition=create"

```

TEST