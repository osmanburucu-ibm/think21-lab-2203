# final pipeline for sum-app

stages:
- build
- deploy
- test

variables:
  UCD_COMPONENT_BASE_PATH: "target"
  UCD_DEPLOY_JSON_PATH: "urbancode"

build_maven:
  stage: build
  image: maven:latest
  artifacts:
    paths:
    - target/sumapp.war
  script:
  - mvn install

unit_test:
  stage: build
  image: maven:latest
  script:
  - mvn test

deploy_using_ucd:
  stage: deploy
  image: osmanburucuibm/udclient:7.1.2.1
  script:
   - echo "Integrating with IBM UrbanCode Deploy..."
   - udclient -weburl $UCD_WEB_URL -authtoken $UCD_AUTH_TOKEN createVersion -component Sum-App-WAR -name $CI_PIPELINE_IID --importing true
   - udclient -weburl $UCD_WEB_URL -authtoken $UCD_AUTH_TOKEN addVersionFiles -component Sum-App-WAR -version $CI_PIPELINE_IID -base $UCD_COMPONENT_BASE_PATH
   - udclient -weburl $UCD_WEB_URL -authtoken $UCD_AUTH_TOKEN finishedImporting -component Sum-App-WAR -version $CI_PIPELINE_IID 
   - udclient -weburl $UCD_WEB_URL -authtoken $UCD_DEPLOY_TOKEN requestApplicationProcess $UCD_DEPLOY_JSON_PATH/deploy.json


sast:
  stage: test
include:
#  - template: Jobs/Code-Quality.gitlab-ci.yml  
  - template: Jobs/Code-Intelligence.gitlab-ci.yml  
  - template: Security/License-Scanning.gitlab-ci.yml
  - template: Security/Dependency-Scanning.gitlab-ci.yml
  - template: Security/SAST.gitlab-ci.yml
  - template: Security/Secret-Detection.gitlab-ci.yml
