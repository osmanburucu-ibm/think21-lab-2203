# pipeline for sum-app

stages:
- build

build_maven:
  stage: build
  image: maven:latest
  artifacts:
    paths:
    - target/sumapp.war
  script:
  - mvn install

unit_test:
  stage: build
  image: maven:latest
  script:
  - mvn test
  
