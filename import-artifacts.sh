#!/bin/bash
# TODO: react to return values of scripts (1 is fail, 0 is ok)
if [ "$#" -gt  0 ]; then
  scripts/show-help.sh
  exit 1
fi

source scripts/setenv-variables.sh

# import first GitLab artifacts
scripts/import-gitlab-artifacts.sh

# import next IBM UrbanCod eartifacts
scripts/import-urbancode-artifacts.sh
