#!/bin/bash
# setting the environment variables for GitLab and IBM UrbanCode Deploy

export GL_GROUPNAME=${GL_GROUPNAME:-OZThink2021}
export GL_URL=${GL_URL:-"https://gitlab.com"}

if [ -z "${GL_PRIVATETOKEN}" ]; then 
  echo "Please provide your private token for environment variable GL_PRIVATETOKEN";
  exit 1;
fi

export DS_USERNAME=${DS_USERNAME:-admin}
export DS_PASSWORD=${DS_PASSWORD:-admin}
export DS_WEB_URL=${DS_WEB_URL:-"https://192.168.72.138:8443"}

if [ -z "${DS_AUTH_TOKEN}" ]; then 
  echo "Please provide your private token for environment variable DS_AUTH_TOKEN";
  exit 1;
fi
