#!/bin/bash

# TODO: instead of username/password provide means for using auth-token!

# check if used variables are set
if [ -z "${DS_USERNAME}" ]; then 
   echo "Username not set"
   exit 1
fi 
if [ -z "${DS_PASSWORD}" ]; then 
   echo "Password not set"
   exit 1
fi 
# check if used variables are set
if [ -z "${DS_WEB_URL}" ]; then 
   echo "IBM UrbanCode Deploy URL not set"
   exit 1
fi 
if [ -z "${DS_AUTH_TOKEN}" ]; then 
   echo "IBM UrbanCode Deploy token not set"
   exit 1
fi 

# download needed plugins
wget https://github.com/UrbanCode/IBM-UCD-PLUGINS/raw/main/files/WebSphereLiberty/ucd-WebSphereLiberty-22.1134888.zip
wget https://github.com/UrbanCode/IBM-UCD-PLUGINS/raw/main/files/UrbancodeVFS/ucd-UrbancodeVFS-43.1139365.zip

# import plugins 
curl -v -k -u "${DS_USERNAME}:${DS_PASSWORD}" -X POST -H "Content-Type: multipart/form-data" -F "file=@ucd-WebSphereLiberty-22.1134888.zip" "${DS_WEB_URL}/rest/plugin/automationPlugin"

curl -v -k -u "${DS_USERNAME}:${DS_PASSWORD}" -X POST -H "Content-Type: multipart/form-data" -F "file=@ucd-UrbancodeVFS-43.1139365.zip "${DS_WEB_URL}/rest/plugin/automationPlugin" 

# import now the components
curl -v -k -u "${DS_USERNAME}:${DS_PASSWORD}" -X PUT -H "Content-Type: multipart/form-data" --header "Accept: application/json" -d "@../urbancode_artifacts/Sum-App_components.json" "${DS_WEB_URL}/cli/component/import?disposition=create"
