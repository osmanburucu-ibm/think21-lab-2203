#!/bin/bash

# check if used variables are set
if [ -z "${GL_GROUPNAME}" ]; then 
   echo "New Groupname not set"
   exit 1
fi 
# check if used variables are set
if [ -z "${GL_URL}" ]; then 
   echo "GitLab URL not set"
   exit 1
fi 
if [ -z "${GL_PRIVATETOKEN}" ]; then 
   echo "GitLab private token not set"
   exit 1
fi 

# create new group from exported file
echo "Import new group $GL_GROUPNAME"
curl --request POST --header "PRIVATE-TOKEN: ${GL_PRIVATETOKEN}" --form "name=${GL_GROUPNAME}" --form "path=${GL_GROUPNAME}" --form "file=@../gitlab_artifacts/think2021_group_export.tar.gz" "$GL_URL/api/v4/groups/import"
# TODO: check if message is 202 Accepted if not exit shell script with error

sleep 30

# WAIT a few seconds to finish import
# if needed get the new Group ID
# NEWGROUPID=$(curl --header "PRIVATE-TOKEN: ${GL_PRIVATETOKEN}" "${GL_URL}"/api/v4/groups -k | jq '.[] | select (.name == "'"$GL_GROUPNAME"'" ).id')

# import now the repository projects into the group
echo "Import Sum-App-Dev into new group"
curl --request POST --header "PRIVATE-TOKEN: ${GL_PRIVATETOKEN}" --form "namespace=${GL_GROUPNAME}" --form "name=sum-app-dev" --form "path=sum-app-dev" --form "file=@../gitlab_artifacts/think2021_sum-app-dev_export.tar.gz" "$GL_URL/api/v4/projects/import"
# TODO: check if message is OK (no error) else exit shell script with error

echo "Import Sum-App-Test into new group"
curl --request POST --header "PRIVATE-TOKEN: ${GL_PRIVATETOKEN}" --form "namespace=${GL_GROUPNAME}" --form "name=sum-app-test" --form "path=sum-app-test" --form "file=@../gitlab_artifacts/think2021_sum-app-test_export.tar.gz" "$GL_URL/api/v4/projects/import"
# TODO: check if message is OK (no error) else exit shell script with error
